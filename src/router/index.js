import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import GameView from '../views/GameView.vue'
import GameKanjiView from '../views/GameKanjiView.vue'
import GameVocabularyView from '../views/GameVocabularyView.vue'
import GameDecksView from '../views/GameDecksView.vue'
import GameListeningView from '../views/GameListeningView.vue'
import GameGrammarView from '../views/GameGrammarView.vue'
import SettingsView from '../views/SettingsView.vue'
import ResultView from '../views/ResultView.vue'
import StudyView from '../views/StudyView.vue'
import StudyKanjiView from '../views/StudyKanjiView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView
    },
    {
      path: '/study',
      name: 'study',
      component: StudyView
    },
    {
      path: '/study-kanji',
      name: 'studykanji',
      component: StudyKanjiView
    },
    {
      path: '/settings',
      name: 'settings',
      component: SettingsView
    },
    {
      path: '/game',
      name: 'game',
      component: GameView
    },
    {
      path: '/game-kanji',
      name: 'gamekanji',
      component: GameKanjiView
    },
    {
      path: '/game-vocabulary',
      name: 'gamevocabulary',
      component: GameVocabularyView
    },
    {
      path: '/game-grammar',
      name: 'gamegrammar',
      component: GameGrammarView
    },
    
    {
      path: '/game-decks',
      name: 'gamedecks',
      component: GameDecksView
    },
    {
      path: '/result',
      name: 'result',
      component: ResultView
    },
    {
      path: '/game-listening',
      name: 'listening',
      component: GameListeningView
    },
    
  ]
})

export default router
